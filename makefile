default:
	gcc src/*.c -o bin/EmojiMan.x86_64 -std=c99 -O2 -Wall -Wno-missing-braces -L lib/ -I include/ -lraylib -ldl -lX11 -lm -lpthread -lrt
windows:
	gcc src/*.c -o bin/EmojiMan.exe -std=c99 -O2 -Wall -Wno-missing-braces -L libwindows/ -I include/ -lraylib -lm -lpthread -lOle32 -lOleaut32 -lWinmm -lVersion -lgdi32 -lopengl32 -mwindows
web:
	emcc src/*.c -o bin/web/eman.html -std=c99 -O2 -Wall -Wno-missing-braces -L libweb/ -I include/ -lraylib -ldl -lX11 -lm -lpthread -lrt -lglfw
webbetter:
	emcc src/*.c -o bin/web/index.html -I ./include -L ./libweb -D_DEFAULT_SOURCE -DEMOJIMAN_OS_WASM -lraylib -lglfw -lm -std=c99 -s USE_GLFW=3 -sLLD_REPORT_UNDEFINED --emrun -s EXPORTED_RUNTIME_METHODS=ccall -s ASYNCIFY -sTOTAL_MEMORY=200MB -s FORCE_FILESYSTEM=1 -DPLATFORM_WEB --preload-file res@res --shell-file=./eman_shell.html