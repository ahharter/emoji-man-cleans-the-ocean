#version 330

// Input vertex attributes (from vertex shader)
in vec2 fragTexCoord;
in vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;
uniform float time;

// Output fragment color
out vec4 finalColor;

// NOTE: Add here your custom variables

const vec2 size = vec2(800, 450);   // Framebuffer size
const float samples = 5.0;          // Pixels per axis; higher = bigger glow, worse performance
const float quality = 2.5;          // Defines size factor: Lower = smaller glow, better quality


vec2 hash( vec2 p ) // replace this by something better
{
	p = vec2( dot(p,vec2(127.1,311.7)), dot(p,vec2(269.5,183.3)) );
	return -1.0 + 2.0*fract(sin(p)*43758.5453123);
}

float noise( in vec2 p )
{
    const float K1 = 0.366025404; // (sqrt(3)-1)/2;
    const float K2 = 0.211324865; // (3-sqrt(3))/6;

	vec2  i = floor( p + (p.x+p.y)*K1 );
    vec2  a = p - i + (i.x+i.y)*K2;
    float m = step(a.y,a.x); 
    vec2  o = vec2(m,1.0-m);
    vec2  b = a - o + K2;
	vec2  c = a - 1.0 + 2.0*K2;
    vec3  h = max( 0.5-vec3(dot(a,a), dot(b,b), dot(c,c) ), 0.0 );
	vec3  n = h*h*h*h*vec3( dot(a,hash(i+0.0)), dot(b,hash(i+o)), dot(c,hash(i+1.0)));
    return dot( n, vec3(70.0) );
}

void main()
{
    float centerPos = 400;
    float waterIntensity = 0.01;

    vec2 p = fragTexCoord.xy / 5;
    vec2 waterUV = p*vec2(2, 3) + time*0.0001;
    float f = 0.0;

    f = noise(24.0 * waterUV);
    f = 0.5 + 0.5*f;

    vec3 screenColor = texture(texture0, fragTexCoord).rgb;
    vec3 waterColor = texture(texture0, vec2(fragTexCoord.x - f*waterIntensity, centerPos - (fragTexCoord.y - centerPos))).rgb;
    vec4 interColor;
    vec4 tintColor = vec4(0.16, 0.93, 1, 1);

    if (fragTexCoord.y*800 < centerPos)
    {
        if (fragTexCoord.y*800 > 10 && fragTexCoord.x*800 > 7 && fragTexCoord.x*800 < 793)
        {
            screenColor = texture(texture0, fragTexCoord - f*waterIntensity).rgb;
        }
        

        interColor = vec4(mix(screenColor, waterColor, fragTexCoord.y), 1);

        finalColor = mix(interColor, tintColor, 0.2);
        
    } else
    {
        finalColor = vec4(screenColor, 1);
    }


}