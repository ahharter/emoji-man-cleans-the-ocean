#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include <raymath.h>
#include <string.h>

#include "game.h"
#include "utils.h"

enum GameState {STATE_TITLE, STATE_GAME, STATE_DEATH} currentState;

int score = 0;
int highScore = 0;

const int fishPenalty = 5;
const int trashGain = 1;

int emojiPosition = 0;
const int emojiSpeed = 4;

const int duckSpawnTime = 340;
int ducksCounter = 0;

const int fishSpawnTime = 45;
int fishCounter = 0;

#define MAX_DUCKS 15
#define MAX_FISH 30

typedef struct Duck {
    bool spawned;
    bool moving;
    Vector2 pos;
    Vector2 size;
    int speed;
} Duck;

Duck ducks[MAX_DUCKS] = {false, false, 800, 20, 100, 80, 2};

typedef struct AquaticEntity {
    bool spawned;
    bool moving;
    Vector2 pos;
    Vector2 size;
    int speed;
    bool isFish;
    int variant;
} AquaticEntity;

AquaticEntity fish[MAX_FISH] = {false, false, 800, 800, 100, 100, 4, false, 0};

typedef struct EmojiMan {
    int speed;
    Vector2 pos;
    Vector2 size;
} EmojiMan;

EmojiMan eMan = {3, 20, 150, 200, 100};

typedef struct EmojiLaser {
    bool active;
    Vector2 startPos;
    Vector2 endPos;
    float width;
} EmojiLaser;

EmojiLaser eLaser = {false, 0, 0, 0, 0, 7};

//Declare Textures
Texture2D titleTexture;
Texture2D deathTexture;

Texture2D instructionTexture;

Texture2D background;

Texture2D bottleTexture;
Texture2D glassTexture;
Texture2D canTextureRed;
Texture2D canTextureGreen;
Texture2D canTextureBlue;

Texture2D fishTextureOrange;
Texture2D fishTextureYellow;
Texture2D fishTexturePurple;
Texture2D fishTextureGray;
Texture2D fishTextureRed;

Texture2D emojiFlyingTexture;

Texture2D duckTexture;

RenderTexture2D waterTarget;

//Declare Shaders
Shader waterShader;

int waterTimeLoc;
float waterTime;

//Declare Sounds
Sound levelMusic;
Sound titleMusic;
Sound deathMusic;

Sound pointSound;
Sound penaltySound;


//Declare Fonts
Font comicRegular;

void InitGame()
{
    printf(GetWorkingDirectory());
    //Load Textures
    titleTexture = LoadTexture("res/textures/emojiTitle.png");
    deathTexture = LoadTexture("res/textures/emojiDeath.png");

    instructionTexture = LoadTexture("res/textures/instructions.png");

    background = LoadTexture("res/textures/bg.png");

    bottleTexture = LoadTexture("res/textures/botle.png");
    glassTexture = LoadTexture("res/textures/glass.png");
    canTextureRed = LoadTexture("res/textures/can_red.png");
    canTextureGreen = LoadTexture("res/textures/can_green.png");
    canTextureBlue = LoadTexture("res/textures/can_blue.png");

    fishTextureOrange = LoadTexture("res/textures/fish_orange.png");
    fishTextureYellow = LoadTexture("res/textures/fish_yellow.png");
    fishTexturePurple = LoadTexture("res/textures/fish_purple.png");
    fishTextureGray = LoadTexture("res/textures/fish_gray.png");
    fishTextureRed = LoadTexture("res/textures/fish_red.png");

    emojiFlyingTexture = LoadTexture("res/textures/emojiFlying.png");

    duckTexture = LoadTexture("res/textures/duck.png");

    waterTarget = LoadRenderTexture(GetScreenWidth(), GetScreenHeight());

    //Load Shaders
    waterShader = LoadShader(0, TextFormat("res/shaders/water%d.glsl", EMOJIMAN_SHADER_VERSION));

    waterTimeLoc = GetShaderLocation(waterShader, "time");

    //Load Sounds
    levelMusic = LoadSound("res/audio/levelMusic.wav");
    titleMusic = LoadSound("res/audio/titleMusic.wav");
    deathMusic = LoadSound("res/audio/deathMusic.wav");

    pointSound = LoadSound("res/audio/point.wav");
    penaltySound = LoadSound("res/audio/penalty.wav");

    //Load Fonts
    comicRegular = LoadFontEx("res/fonts/ComicNeue-Bold.ttf", 32, 0, 250);

    //Initialize entities
    int i;
    for (i=0;i<MAX_DUCKS;i++)
    {
        ducks[i].size.x = 100;
        ducks[i].size.y = 80;
        ducks[i].speed = 4;
    }

    int fj;
    for (fj=0;fj<MAX_FISH;fj++)
    {
        fish[fj].size.x = 100;
        fish[fj].size.y = 100;
        fish[fj].speed = 4;
        fish[fj].variant = 0;
    }
}

void UpdateGame(int frameCount, float deltaTime)
{
    if (IsKeyDown(KEY_ESCAPE))
    {
        ExitToTitle();
    }
    if (currentState == STATE_GAME)
    {
        waterTime += 10;
        SetShaderValue(waterShader, waterTimeLoc, &waterTime, SHADER_UNIFORM_FLOAT);
        if (!IsSoundPlaying(levelMusic))
        {
            PlaySound(levelMusic);
        }

        // Move Emoji Man
        if (IsKeyDown(KEY_UP) || IsKeyDown(KEY_W)) eMan.pos.y -= eMan.speed;
        if (IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_S)) eMan.pos.y += eMan.speed;
        eMan.pos.y = clampInt(eMan.pos.y, 20, GetScreenHeight() / 2 - eMan.size.y);

        if (IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_A)) eMan.pos.x -= eMan.speed;
        if (IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_D)) eMan.pos.x += eMan.speed;
        eMan.pos.x = clampInt(eMan.pos.x, 20, GetScreenWidth() - 20 - eMan.size.x);


        // Spawn the Ducks
        if (frameCount % duckSpawnTime == 0)
        {
            printf("[MODTIME] SPAWNED: %d   MOVING: %d   POSX: %f\n", ducks[ducksCounter].spawned, ducks[ducksCounter].moving, ducks[ducksCounter].pos.x);
            if (!ducks[ducksCounter].spawned)
            {
                ducks[ducksCounter].pos.x = 800;
                int duckRail = rand() % 4;
                printf("RANDOM VALUE: %d\n", duckRail);
                switch (duckRail)
                {
                case 0:
                    ducks[ducksCounter].spawned = true;
                    ducks[ducksCounter].moving = true;
                    ducks[ducksCounter].pos.y = 20;
                    break;
                
                case 1:
                    ducks[ducksCounter].spawned = true;
                    ducks[ducksCounter].moving = true;
                    ducks[ducksCounter].pos.y = 202 + 20;
                    break;

                default:
                    ducks[ducksCounter].moving = false;
                    ducks[ducksCounter].spawned = false;
                }

            }

            // I think this boi is causing previously ignored ducks to not show up in the same index in subsequent loops I added a possible fix on the previous changed lines
            //ducks[ducksCounter].spawned = true;
            

            printf("COUNT: %d  VALUE: %f\n", ducksCounter, ducks[ducksCounter].pos.x);
            ducksCounter++;
            if (ducksCounter >= MAX_DUCKS)
            {
                ducksCounter = 0;
            }
            printf("[POSTTIME] SPAWNED: %d   MOVING: %d   POSX: %f\n", ducks[ducksCounter].spawned, ducks[ducksCounter].moving, ducks[ducksCounter].pos.x);
        }

        //Spawn the fish/trash
        if (frameCount % fishSpawnTime == 0 || frameCount % fishSpawnTime == 1 )
        {
            if (!fish[fishCounter].spawned)
            {
                fish[fishCounter].pos.x = 800;
                int fishRail = rand() % 5;

                if (fishCounter % 2 == 0)
                {
                    fish[fishCounter].pos.y = (GetScreenHeight() / 2) + 70;
                } else
                {
                    fish[fishCounter].pos.y = (GetScreenHeight() / 2) + 150 + 70;
                }

                fish[fishCounter].spawned = true;
                fish[fishCounter].moving = true;

                switch (fishRail)
                {
                case 0:
                    fish[fishCounter].isFish = true;
                    break;
                
                default:
                    fish[fishCounter].isFish = false;
                    break;
                }

                fish[fishCounter].variant = rand() % 5;
            }


            fishCounter++;
            if (fishCounter >= MAX_FISH)
            {
                fishCounter = 0;
            }
        }

        //Update ducks
        int i;
        for (i=0;i<MAX_DUCKS;i++)
        {
            if (ducks[i].moving)
            {
                ducks[i].pos.x -= ducks[i].speed;
            }
            if (ducks[i].pos.x < -100)
            {
                ducks[i].spawned = false;
            }
        }

        //Update fish
        int j;
        for (j=0;j<MAX_FISH;j++)
        {
            if (fish[j].moving)
            {
                fish[j].pos.x -= fish[j].speed;
            }
            if (fish[j].pos.x < -100)
            {
                fish[j].spawned = false;
            }
        }

        //Update the Emoji Laser
        Vector2 laserOffset = {180, 50};
        eLaser.startPos = Vector2Add(eMan.pos, laserOffset);
        eLaser.endPos = Vector2Scale(Vector2Subtract(GetMousePosition(), eLaser.startPos), 1000);
        if (IsMouseButtonDown(0))
        {
            eLaser.active = true;
        } else
        {
            eLaser.active = false;
        }

        //Check for laser collisions
        int c;
        for (c=0;c<MAX_FISH;c++)
        {
            if (fish[c].spawned && eLaser.active)
            {
                if (IntersectLines(eLaser.startPos, eLaser.endPos, (Vector2){fish[c].pos.x + fish[c].size.x, fish[c].pos.y}, (Vector2){fish[c].pos.x, fish[c].pos.y + fish[c].size.y}) || IntersectLines(eLaser.startPos, eLaser.endPos, fish[c].pos, Vector2Add(fish[c].pos, fish[c].size)))
                {
                    //Laser Collision
                    printf("[Laser Intersection at %d]\n", frameCount);
                    fish[c].spawned = false;
                    if (fish[c].isFish)
                    {
                        score -= fishPenalty;
                        PlaySoundMulti(penaltySound);
                    } else 
                    {
                        score += trashGain;
                        PlaySoundMulti(pointSound);
                    }

                }
            }
        }

        if (score < 0)
        {
            score = 0;
        }

        //Check for Duck Collisions
        int e;
        for (e=0;e<MAX_DUCKS;e++)
        {
            if (ducks[e].spawned)
            {
                if (IntersectRect(ducks[e].pos.x, ducks[e].pos.y, ducks[e].size.x, ducks[e].size.y, eMan.pos.x, eMan.pos.y, eMan.size.x, eMan.size.y))
                {
                    //Intersection detected
                    Die();
                } 
            }
        }
    } else
    {
        if (IsKeyDown(KEY_ENTER))
        {
            ResetGame();
        }
    }

    if (currentState == STATE_DEATH)
    {
        if (!IsSoundPlaying(deathMusic))
        {
            PlaySound(deathMusic);
        }
    }
    if (currentState == STATE_TITLE)
    {
        if (!IsSoundPlaying(titleMusic))
        {
            PlaySound(titleMusic);
        }
    }
    

    

}

void RenderGame()
{   
    
    switch (currentState)
    {
    case STATE_TITLE:
    BeginDrawing();
        DrawTexture(titleTexture, 0, 0, WHITE);
        DrawTextEx(comicRegular, "[TAB] Quit   [I] How to play", (Vector2){20, 20}, 30, 1, BLACK);
        DrawTextEx(comicRegular, "[Version 1.2] Made by Volcano339 for SeaJam\nTitle and Death music by LandFill", (Vector2){20, GetScreenHeight() - 79}, 26, 1, BLACK);
        DrawTextEx(comicRegular, "[Version 1.2] Made by Volcano339 for SeaJam\nTitle and Death music by LandFill", (Vector2){20, GetScreenHeight() - 80}, 26, 1, YELLOW);
        if (IsKeyDown(KEY_I))
        {
            DrawTexture(instructionTexture, GetScreenWidth()/2 - 262, GetScreenHeight()/2 - 152, WHITE);
        }
    EndDrawing();
        break;
    case STATE_DEATH: 
    BeginDrawing();
        DrawTexture(deathTexture, 0, 0, WHITE);
        char highScoreDisplay[32];
        sprintf(highScoreDisplay, "Score: %d\nHigh Score: %d", score, highScore);
        DrawTextEx(comicRegular, highScoreDisplay, (Vector2){80, 175}, 40, 1, YELLOW);
        DrawTextEx(comicRegular, "[ESC] Exit", (Vector2){20, 20}, 30, 1, WHITE);
    EndDrawing();
        break;
    default:
        BeginTextureMode(waterTarget);
        ClearBackground(BLUE);
        //Render Background
        DrawTexture(background, 0, 0, WHITE);

        //Render Emoji Laser
        if (eLaser.active)
        {
            
            DrawLineEx(eLaser.startPos, eLaser.endPos, eLaser.width + 8, RED);
            DrawLineEx(eLaser.startPos, eLaser.endPos, eLaser.width, PINK);
        }

        //Render Emoji Man
        DrawTexture(emojiFlyingTexture, eMan.pos.x, eMan.pos.y, WHITE);
        
        //Render ducks
        int i;
        for (i=0;i<MAX_DUCKS;i++)
        {
            if (ducks[i].spawned)
            {
                //printf("[SPAWNED] INDEX: %d   POS(%f, %f)   SIZE(%f, %f)\n", i, ducks[i].pos.x, ducks[i].pos.y, ducks[i].size.x, ducks[i].size.y);
                DrawTexture(duckTexture, ducks[i].pos.x, ducks[i].pos.y, WHITE);
            }
            
        }

        //Render fish
        int j;
        for (j=0;j<MAX_FISH;j++)
        {
            if (fish[j].spawned)
            {
                if (fish[j].isFish)
                {
                    switch (fish[j].variant)
                    {
                    case 0:
                        DrawTexture(fishTextureOrange, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    case 1:
                        DrawTexture(fishTextureYellow, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    case 2:
                        DrawTexture(fishTexturePurple, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    case 3:
                        DrawTexture(fishTextureGray, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    
                    default:
                        DrawTexture(fishTextureRed, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    }
                } else
                {
                    switch (fish[j].variant)
                    {
                    case 0:
                        DrawTexture(bottleTexture, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    case 1:
                        DrawTexture(glassTexture, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    case 2:
                        DrawTexture(canTextureRed, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    case 3:
                        DrawTexture(canTextureGreen, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    
                    default:
                        DrawTexture(canTextureBlue, fish[j].pos.x, fish[j].pos.y, WHITE);
                        break;
                    }
                    //DrawRectangle(fish[j].pos.x, fish[j].pos.y, fish[j].size.x, fish[j].size.y, WHITE);
                }
                
            }
        }
        EndTextureMode();

        BeginDrawing();
        ClearBackground(BLUE);
        //Render Water
        BeginShaderMode(waterShader);
            DrawTextureRec(waterTarget.texture, (Rectangle){ 0, 0, waterTarget.texture.width, -waterTarget.texture.height}, (Vector2){0, 0}, WHITE);
        EndShaderMode();


        //Render HUD
        char scoreDisplay[32];
        sprintf(scoreDisplay, "Score: %d", score);
        DrawTextEx(comicRegular, scoreDisplay, (Vector2){20, 23}, 39, 1, BLACK);
        DrawTextEx(comicRegular, scoreDisplay, (Vector2){20, 20}, 39, 1, YELLOW);

        EndDrawing();
        break;
    }
    
        
        
    
}

void DeInit()
{
    //Unload Textures
    UnloadTexture(background);

    //Unload Fonts
    UnloadFont(comicRegular);
}

void ResetGame()
{
    StopSound(deathMusic);
    StopSound(titleMusic);
    currentState = STATE_GAME;
    score = 0;

    int i;
    for (i=0;i<MAX_FISH;i++)
    {
        fish[i].spawned = false;
    }

    int j;
    for (j=0;j<MAX_DUCKS;j++)
    {
        ducks[j].spawned = false;
    }

    eMan.pos = (Vector2){20, 150};

}

void Die()
{
    if (score > highScore)
    {
        highScore = score;
    }
    currentState = STATE_DEATH;
    StopSound(levelMusic);
    StopSound(titleMusic);
}

void ExitToTitle()
{
    StopSound(deathMusic);
    StopSound(levelMusic);
    currentState = STATE_TITLE;
}

//TODO: HTML Port
//TODO: Better Background
//FIXME: Window Scalability