#pragma once

#ifdef EMOJIMAN_OS_WASM
#include <emscripten.h>
#define EMOJIMAN_SHADER_VERSION 1
#else
#define EMOJIMAN_SHADER_VERSION 3
#endif

void InitGame();
void UpdateGame(int frameCount, float deltaTime);
void RenderGame();
void DeInit();
void ResetGame();
void Die();
void ExitToTitle();