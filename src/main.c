#include <raylib.h>
#include <stdio.h>

#ifdef EMOJIMAN_OS_WASM
#include <emscripten.h>
#define EMOJIMAN_SHADER_VERSION 1
#else
#define EMOJIMAN_SHADER_VERSION 3
#endif

#include "game.h"

int g_frameCount = 0;

void FrameCallback()
{
	UpdateGame(g_frameCount, GetFrameTime());
    RenderGame();

    g_frameCount++;
}

int main(void)
{
    Image windowIcon = LoadImage("res/textures/icon.png");
    const int windowWidth = 800;
    const int windowHeight = 800;
    InitWindow(windowWidth, windowHeight, "Emoji Man Cleans the Ocean");
    SetWindowIcon(windowIcon);

    InitAudioDevice();
    InitGame();

    SetExitKey(KEY_TAB);
    SetMasterVolume(0.15);
    SetTargetFPS(60);

	#ifdef EMOJIMAN_OS_WASM
	emscripten_set_main_loop(FrameCallback, 0, 1);
	#else
    while (!WindowShouldClose())
    {
        FrameCallback();
    }
	#endif

    DeInit();
    CloseAudioDevice();
    CloseWindow();
    return 0;
}

