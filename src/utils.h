#pragma once

int clampInt(int d, int min, int max) {
  const int t = d < min ? min : d;
  return t > max ? max : t;
}


//We're supposed to add the position to the size to get the global points (this might have already done it)
bool IntersectRect(float xa, float ya, float wa, float ha, float xb, float yb, float wb, float hb)
{
  return (
    xa < xb + wb &&
    xa + wa > xb &&
    ya < yb + hb &&
    ha + ya > yb
    );
}

bool IntersectLines(Vector2 A, Vector2 B, Vector2 C, Vector2 D)
	{
		Vector2 CmP = {C.x - A.x, C.y - A.y};
		Vector2 r = {B.x - A.x, B.y - A.y};
		Vector2 s = {D.x - C.x, D.y - C.y};
 
		float CmPxr = CmP.x * r.y - CmP.y * r.x;
		float CmPxs = CmP.x * s.y - CmP.y * s.x;
		float rxs = r.x * s.y - r.y * s.x;
 
		if (CmPxr == 0.f)
		{
			// Lines are collinear, and so intersect if they have any overlap
 
			return ((C.x - A.x < 0.f) != (C.x - B.x < 0.f))
				|| ((C.y - A.y < 0.f) != (C.y - B.y < 0.f));
		}
 
		if (rxs == 0.f)
			return false; // Lines are parallel.
 
		float rxsr = 1.f / rxs;
		float t = CmPxs * rxsr;
		float u = CmPxr * rxsr;
 
		return (t >= 0.f) && (t <= 1.f) && (u >= 0.f) && (u <= 1.f);
	}